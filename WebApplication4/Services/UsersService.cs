﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.Interfaces;
using System.Data.Entity;
using System.Net.Mail;
using System.Net;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.IdentityModel.Tokens;

namespace WebApplication4.Services
{
    public class UsersService : IUsers
    {
        aspnetdbEntities _context;
        private static string Secret = "ERMN05OPLoDvbTTa/QkqLNMI7cPLguaRyHzyg7n5qNBVjQmtBhz4SzYh4NBVCXi3KJHlSXKP+oi2+bXr6CUYTR==";
        public UsersService(aspnetdbEntities context)
        {
            _context = context;

        }

        public void DeleteUserData(string id)
        {
            try
            {
                var data = _context.aspnet_Users.FirstOrDefault(c => c.UserId.ToString() == id);
                if (data != null)
                {
                    _context.aspnet_Users.Remove(data);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<aspnet_Users> GetAllUsers()
        {
            var data = _context.aspnet_Users.Include(c => c.aspnet_Applications); // get all data 
            return data.ToList();
        }

        public aspnet_Users GetById(string id)
        {
            var data = _context.aspnet_Users.FirstOrDefault(c => c.ApplicationId.ToString() == id);
            return data;
        }

        public void PostUsersData(aspnet_Users value)
        {
            try
            {
                _context.aspnet_Users.Add(new aspnet_Users
                {
                    ApplicationId = value.ApplicationId,
                    UserName = value.UserName,
                    LoweredUserName = value.LoweredUserName,
                    MobileAlias = value.MobileAlias,
                    IsAnonymous = value.IsAnonymous,
                    LastActivityDate = DateTime.Now
                }); _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PutUserData(aspnet_Users value)
        {
            var data = _context.aspnet_Users.FirstOrDefault(c => c.UserId == value.UserId);
            if (data != null)
            {
                data.ApplicationId = value.ApplicationId;
                data.UserName = value.UserName;
                data.LoweredUserName = value.LoweredUserName;
                data.MobileAlias = value.MobileAlias;
                data.IsAnonymous = value.IsAnonymous;
                data.LastActivityDate = DateTime.Now;

                _context.SaveChanges();
            }
        }

        public void ForgotPassword(string userEmail, string securityQuestion, string securityAnswer)
        {
            try
            {
                var data = _context.aspnet_Membership.FirstOrDefault(u => u.Email == userEmail && u.PasswordQuestion == securityQuestion && u.PasswordAnswer == securityAnswer);
                if (data != null)
                {
                    string password = CreateRandomPassword(5);
                    data.Password = password;

                    _context.SaveChanges();

                    //here you can code to send password through mail
                    SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                    client.EnableSsl = true;
                    client.UseDefaultCredentials = false;
                    client.Credentials = new NetworkCredential("members@gmail.com", "members.com");

                    MailMessage mailMessage = new MailMessage();
                    mailMessage.From = new MailAddress("members@gmail.com");
                    mailMessage.To.Add(data.Email);
                    mailMessage.IsBodyHtml = true;
                    mailMessage.Body = "Your New Password is <b>" + " " + password + "</b>";

                    mailMessage.Subject = "Password Reset";
                    client.Send(mailMessage);
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public aspnet_Membership AuthenticateUser(string email, string password)
        {
            var data = _context.aspnet_Membership.SingleOrDefault(m => m.Email == email && m.Password == password);
            if (data == null)
            {
                return null;
            }
            else
            {
                // authentication successful so generate jwt token
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(Secret);
                var tokenDescriptor = new SecurityTokenDescriptor
                {
                    Subject = new ClaimsIdentity(new Claim[]
                    {
                        new Claim(ClaimTypes.Name, data.UserId.ToString())
                    }),
                    Expires = DateTime.UtcNow.AddMinutes(5),
                    SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                };
                var token = tokenHandler.CreateToken(tokenDescriptor);
                data.Token = tokenHandler.WriteToken(token);
                data.Password = null;
                return data;
            }
        }

        private static string CreateRandomPassword(int passwordLength)
        {
            string allowedChars = "abcdefghijkmnopqrstuvwxyzABCDEFGHJKLMNOPQRSTUVWXYZ0123456789!@$?_-";
            char[] chars = new char[passwordLength];
            Random rd = new Random();
            for (int i = 0; i < passwordLength; i++)
            {
                chars[i] = allowedChars[rd.Next(0, allowedChars.Length)];
            }

            return new string(chars);
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~UsersService()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}