﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.Interfaces;

namespace WebApplication4.Services
{
    public class ApplicationService : IApplication
    {
        aspnetdbEntities _context;
        public ApplicationService(aspnetdbEntities context)
        {
            _context = context;
        }

        public void DeleteApplicationData(string id)
        {
            try
            {
                var data = _context.aspnet_Applications.FirstOrDefault(c => c.ApplicationId.ToString() == id);
                if (data != null)
                {
                    _context.aspnet_Applications.Remove(data);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<aspnet_Applications> GetAllApplication()
        {
            var data = _context.aspnet_Applications; // get all data 
            return data.ToList();
        }

        public aspnet_Applications GetById(string id)
        {
            var data = _context.aspnet_Applications.FirstOrDefault(c => c.ApplicationId.ToString() == id);
            return data;
        }

        public void PostApplicationData(aspnet_Applications values)
        {
            try
            {
                _context.aspnet_Applications.Add(new aspnet_Applications
                {
                    ApplicationName = values.ApplicationName,
                    LoweredApplicationName = values.LoweredApplicationName,
                    Description = values.Description
                }); _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PutApplicationData(aspnet_Applications value)
        {
            var data = _context.aspnet_Applications.FirstOrDefault(c => c.ApplicationId == value.ApplicationId);
            if (data != null)
            {
                data.ApplicationName = value.ApplicationName;
                data.LoweredApplicationName = value.LoweredApplicationName;
                data.Description = value.Description;

                _context.SaveChanges();
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~ApplicationService()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion

    }
}