﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using WebApplication4.Models;
using WebApplication4.Interfaces;
using System.Data.Entity;

namespace WebApplication4.Services
{
    public class MembershipService : IMembers
    {
        aspnetdbEntities _context;
        public MembershipService(aspnetdbEntities context)
        {
            _context = context;
        }

        public void DeleteMembership(string id)
        {
            try
            {
                var data = _context.aspnet_Membership.FirstOrDefault(c => c.ApplicationId.ToString() == id);
                if (data != null)
                {
                    _context.aspnet_Membership.Remove(data);
                    _context.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public IEnumerable<aspnet_Membership> GetAllMemberships()
        {
            var data = _context.aspnet_Membership.Include(a => a.aspnet_Applications).Include(u => u.aspnet_Users); // get all data 
            return data.ToList();
        }

        public aspnet_Membership GetById(string id)
        {
            var data = _context.aspnet_Membership.FirstOrDefault(c => c.ApplicationId.ToString() == id);
            return data;
        }

        public void PostMembership(aspnet_Membership values)
        {
            try
            {
                _context.aspnet_Membership.Add(new aspnet_Membership
                {
                    ApplicationId = values.ApplicationId,
                    UserId = values.UserId,
                    Password = values.Password,
                    PasswordFormat = values.PasswordFormat,
                    PasswordSalt = values.PasswordSalt,
                    MobilePIN = values.MobilePIN,
                    Email = values.Email,
                    LoweredEmail = values.LoweredEmail,
                    PasswordQuestion = values.PasswordQuestion,
                    PasswordAnswer = values.PasswordAnswer,
                    IsApproved = values.IsApproved,
                    IsLockedOut = values.IsLockedOut,
                    CreateDate = DateTime.Now,
                    LastLoginDate = DateTime.Now,
                    LastPasswordChangedDate = DateTime.Now,
                    LastLockoutDate = DateTime.Now,
                    FailedPasswordAttemptCount = values.FailedPasswordAttemptCount,
                    FailedPasswordAnswerAttemptWindowStart = values.FailedPasswordAnswerAttemptWindowStart,
                    FailedPasswordAnswerAttemptCount = values.FailedPasswordAnswerAttemptCount,
                    Comment = values.Comment
                }); _context.SaveChanges();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public void PutMembership(aspnet_Membership values)
        {
            var data = _context.aspnet_Membership.FirstOrDefault(c => c.UserId == values.UserId && c.ApplicationId == values.ApplicationId);
            if (data != null)
            {
                data.ApplicationId = values.ApplicationId;
                data.UserId = values.UserId;
                data.Password = values.Password;
                data.PasswordFormat = values.PasswordFormat;
                data.PasswordSalt = values.PasswordSalt;
                data.MobilePIN = values.MobilePIN;
                data.Email = values.Email;
                data.LoweredEmail = values.LoweredEmail;
                data.PasswordQuestion = values.PasswordQuestion;
                data.PasswordAnswer = values.PasswordAnswer;
                data.IsApproved = values.IsApproved;
                data.IsLockedOut = values.IsLockedOut;
                data.CreateDate = DateTime.Now;
                data.LastLoginDate = DateTime.Now;
                data.LastPasswordChangedDate = DateTime.Now;
                data.LastLockoutDate = DateTime.Now;
                data.FailedPasswordAttemptCount = values.FailedPasswordAttemptCount;
                data.FailedPasswordAnswerAttemptWindowStart = values.FailedPasswordAnswerAttemptWindowStart;
                data.FailedPasswordAnswerAttemptCount = values.FailedPasswordAnswerAttemptCount;
                data.Comment = values.Comment;

                _context.SaveChanges();
            }
        }

        #region IDisposable Support
        private bool disposedValue = false; // To detect redundant calls

        protected virtual void Dispose(bool disposing)
        {
            if (!disposedValue)
            {
                if (disposing)
                {
                    // TODO: dispose managed state (managed objects).
                }

                // TODO: free unmanaged resources (unmanaged objects) and override a finalizer below.
                // TODO: set large fields to null.

                disposedValue = true;
            }
        }

        // TODO: override a finalizer only if Dispose(bool disposing) above has code to free unmanaged resources.
        // ~MembershipService()
        // {
        //   // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
        //   Dispose(false);
        // }

        // This code added to correctly implement the disposable pattern.
        public void Dispose()
        {
            // Do not change this code. Put cleanup code in Dispose(bool disposing) above.
            Dispose(true);
            // TODO: uncomment the following line if the finalizer is overridden above.
            // GC.SuppressFinalize(this);
        }
        #endregion
    }
}