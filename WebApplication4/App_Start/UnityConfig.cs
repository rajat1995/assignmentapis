using System.Web.Http;
using Unity;
using Unity.WebApi;
using WebApplication4.Interfaces;
using WebApplication4.Services;

namespace WebApplication4
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
            var container = new UnityContainer();

            //container.RegisterType<IUsers, UsersService>();

            // register all your components with the container here
            // it is NOT necessary to register your controllers

            // e.g. container.RegisterType<ITestService, TestService>();

            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}