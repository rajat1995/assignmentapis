﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.Interfaces
{
    public interface IUsers : IDisposable
    {
        IEnumerable<aspnet_Users> GetAllUsers();
        aspnet_Users GetById(string id);
        void PostUsersData(aspnet_Users value);
        void PutUserData(aspnet_Users value);
        void ForgotPassword(string userEmail, string securityQuestion, string securityAnswer);
        void DeleteUserData(string id);
        aspnet_Membership AuthenticateUser(string email, string password);
    }
}
