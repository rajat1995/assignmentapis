﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.Interfaces
{
    public interface IApplication : IDisposable
    {
        IEnumerable<aspnet_Applications> GetAllApplication();
        aspnet_Applications GetById(string id);
        void PostApplicationData(aspnet_Applications values);
        void PutApplicationData(aspnet_Applications values);
        void DeleteApplicationData(string id);
    }
}
