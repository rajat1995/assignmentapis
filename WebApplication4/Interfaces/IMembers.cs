﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using WebApplication4.Models;

namespace WebApplication4.Interfaces
{
    public interface IMembers : IDisposable
    {
        IEnumerable<aspnet_Membership> GetAllMemberships();
        aspnet_Membership GetById(string id);
        void PostMembership(aspnet_Membership values);
        void PutMembership(aspnet_Membership values);
        void DeleteMembership(string id);
    }
}
