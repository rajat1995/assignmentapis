﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using WebApplication4.Interfaces;
using WebApplication4.Models;

namespace WebApplication4.Controllers
{
    //[Route("api/Application")]
    public class ApplicationController : ApiController
    {
        private readonly IApplication _application;
        public ApplicationController(IApplication application)
        {
            _application = application;
        }
        // GET: api/Application
        //[HttpGet]
        //[Route("getAllApplications")]
        public IEnumerable<aspnet_Applications> GetAll()
        {
            return _application.GetAllApplication();
        }

        // GET: api/Application/5
        //[HttpGet]
        //[Route("getById")]
        public IHttpActionResult Get(string id)
        {
            var items = _application.GetById(id);
            if (items == null)
            {
                return NotFound();
            }
            return Ok(items);
        }

        //// POST: api/Application
        //[HttpPost]
        //[Route("postApplication")]
        public IHttpActionResult Post([FromBody]aspnet_Applications values)
        {
            if (values == null)
            {
                return null;
            }
            _application.PostApplicationData(values);
            return Ok(new { message = "Data post successfully" });
        }

        //// PUT: api/Application/5
        //[HttpPut]
        //[Route("putApplication")]
        public IHttpActionResult Put([FromBody]aspnet_Applications values)
        {
            if (values == null)
            {
                return BadRequest();
            }
            _application.PutApplicationData(values);
            return Ok(new { message = "Data put successfully" });
        }

        // DELETE: api/Application/5
        //[HttpDelete]
        //[Route("deleteApplication")]
        public IHttpActionResult Delete(string id)
        {
            _application.DeleteApplicationData(id);
            return Ok(new { message = "Delete data successfully" });
        }

        protected override void Dispose(Boolean disposing)
        {
            _application?.Dispose();
            base.Dispose(disposing);
        }
    }
}
