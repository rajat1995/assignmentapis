﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication4.Models;
using WebApplication4.Interfaces;

namespace WebApplication4.Controllers
{
    public class UsersController : ApiController
    {
        private readonly IUsers _users;
        public UsersController(IUsers users)
        {
            _users = users;
        }
        // GET api/Users
        [HttpGet]
        [Route("api/Users/getAllUsers")]
        public IEnumerable<aspnet_Users> Get()
        {
            return _users.GetAllUsers();
        }

        // GET api/Users/5
        [HttpGet]
        [Route("api/Users/{id}/getById")]
        public IHttpActionResult Get(string id)
        {
            var items = _users.GetById(id);
            if (items == null)
            {
                return NotFound();
            }
            return Ok(items);
        }

        // GET api/Users/5
        [HttpGet]
        [Route("api/Users/{email}/login/{password}")]
        public IHttpActionResult Authentication(string email, string password)
        {
            var items = _users.AuthenticateUser(email, password);
            if (items == null)
            {
                return BadRequest();
            }
            return Ok(items);
        }

        // POST api/Users
        //[HttpPost]
        //[Route("postuser")]
        public IHttpActionResult Post([FromBody]aspnet_Users value)
        {
            if (value == null)
            {
                return NotFound();
            }
            _users.PostUsersData(value);
            return Ok(new { message = "User add successfully" });
        }

        // PUT api/Users/5
        //[HttpPut]
        //[Route("putUser")]
        public IHttpActionResult Put([FromBody]aspnet_Users value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            _users.PutUserData(value);
            return Ok(new { message = "User update successfully" });
        }

        // PUT api/Users/5
        [HttpPut]
        [Route("api/users/{userEmail}/forgot/{securityQuestion}/password/{securityAnswer}")]
        public IHttpActionResult Put(string userEmail, string securityQuestion, string securityAnswer)
        {
            if (userEmail == null || securityQuestion == null || securityAnswer == null)
            {
                return BadRequest();
            }
            _users.ForgotPassword(userEmail, securityQuestion, securityAnswer);
            return Ok(new { message = "Password changed successfully" });
        }

        // DELETE api/Users/5
        [HttpDelete]
        [Route("api/Users/{id}/deleteUser")]
        public IHttpActionResult Delete(string id)
        {
            _users.DeleteUserData(id);
            return Ok(new { message = "Delete data successfully" });
        }

        protected override void Dispose(Boolean disposing)
        {
            _users?.Dispose();
            base.Dispose(disposing);
        }
    }
}
