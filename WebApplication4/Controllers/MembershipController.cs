﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using WebApplication4.Models;
using WebApplication4.Interfaces;

namespace WebApplication4.Controllers
{
    public class MembershipController : ApiController
    {
        private readonly IMembers _member;
        public MembershipController(IMembers member)
        {
            _member = member;
        }
        // GET: api/Membership
        public IEnumerable<aspnet_Membership> Get()
        {
            return _member.GetAllMemberships();
        }

        // GET: api/Membership/5
        public IHttpActionResult Get(string id)
        {
            var items = _member.GetById(id);
            if (items == null)
            {
                return NotFound();
            }
            return Ok(items);
        }

        // POST: api/Membership
        public IHttpActionResult Post([FromBody]aspnet_Membership value)
        {
            if (value == null)
            {
                return NotFound();
            }
            _member.PostMembership(value);
            return Ok(new { message = "membership add successfully" });
        }

        // PUT: api/Membership/5
        public IHttpActionResult Put(int id, [FromBody]aspnet_Membership value)
        {
            if (value == null)
            {
                return BadRequest();
            }
            _member.PutMembership(value);
            return Ok(new { message = "Membership update successfully" });
        }

        // DELETE: api/Membership/5
        public IHttpActionResult Delete(string id)
        {
            _member.DeleteMembership(id);
            return Ok(new { message = "Delete data successfully" });
        }

        protected override void Dispose(Boolean disposing)
        {
            _member?.Dispose();
            base.Dispose(disposing);
        }
    }
}
